# Spring Cloud Config Server Example  

## Prerequisites
* **_JDK 8_** - Install JDK 1.8 version
* **_Maven_** - Download latest version

## Running Instructions

```bash
$ cd spring-cloud-config-server
$ mvn clean install
$ java -jar microservices-config-server/target/microservices-config-server-1.0.jar 
$ java -jar microservices-config-client/target/microservices-config-client-1.0.jar
```
## Usage
### Show property on config server for client app with development profile on master bracnh:
```sh
$ go to http://localhost:8888/client/development/master
```

### Show property loaded and cached on client:
```sh
$ go to http://localhost:8080/whoami/Mr_Pink
```